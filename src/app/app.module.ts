import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';

import { VaucherUnidadComponent } from './pages/vaucher-unidad/vaucher-unidad.component';
import { VaucherUnidadService } from './services/vaucher-unidad.service';
import { VaucherGerencialComponent } from './pages/vaucher-gerencial/vaucher-gerencial.component';




@NgModule({
  declarations: [
    AppComponent,
    VaucherUnidadComponent,
    VaucherGerencialComponent
  ],
  imports: [
    BrowserModule,
    FormsModule


  ],
  providers: [VaucherUnidadService],
  bootstrap: [AppComponent]
})
export class AppModule { }
